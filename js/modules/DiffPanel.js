/**
 * Diff panel for comparing text files
 * Copyright 2013 by Roman Khudyakov, roma.khudyakov@gmail.com
 *
 * Usage
 * >>> var myDiffPanel = new DiffPanel(
 *        document.getElementById('jb-diff-panel'),
 *        [content1, content2],
 *        diff_model,
 *        TextPanelConstructor
 *    );
 *
 *  depends form corresponding css file: CodePanel.css
 */


var DiffPanel = (function () {

	/**    a base css which will be added to all inner elements of html markup */
	var BASE_CSS_CLASS = 'diff-panel';

	/** css classes for markup */
	var CSS_CLASSES = {
		codePanel: '__code-panel'
	};

	/** html for markup */
	var NODES = {
		bod: "<div class='" + BASE_CSS_CLASS + " clear'></div>",
		codePanel: "<div class='" + BASE_CSS_CLASS + CSS_CLASSES.codePanel + "'></div>"
	};

	/** diff model keys */
	var DIFF_KEYS = {
		before_line_number: "before_line_number",
		before_line_count: "before_line_count",
		after_line_number: "after_line_number",
		after_line_count: "after_line_count"
	};

	var LINE_STATUSES = {
		added: 'add',
		deleted: 'del',
		modified: 'mod',
		spacer: 'spacer'
	};

	var config = {
		//diffScrollOffset: 15 // TODO should be calculating as a middle of the screen
	};

	/**
	 *
	 * @param HTMLElement
	 * @param contents
	 * @param diffModel
	 * @param Panel {Function} a text panel function constructor
	 * @constructor
	 */
	function DiffPanel(HTMLElement, contents, diffModel, Panel) {
		this.HTMLElement = HTMLElement;
		this.contents = contents;
		this.diffModel = diffModel;
		this.Panel = Panel;

		this.init();
	}

	DiffPanel.prototype.init = function () {
		var self = this;

		self.nodesContents = [];
		self.codePanels = [];
		self.highlightModel = createHighlightModel(self.diffModel);
		self.posCodePanelsCorrections = {};
		self.beforePanelPosFix = 0;
		self.afterPanelPosFix = 0;


		drawMarkup.call(self);
		initCodePanels.call(self);
		jumpToFirstDiff.call(self);

	};

	var drawMarkup = function () {
		var i = 0,
			len = this.contents.length;

		// create body
		this.nodeBod = H.insertNode(NODES.bod, this.HTMLElement, true)[0];

		// create code panels containers
		for (; i < len; i++) {
			this.nodesContents.push(H.insertNode(NODES.codePanel, this.nodeBod, true)[0]);
		}

	};

	var initCodePanels = function () {
		var i = 0,
			len = this.contents.length,
			width = 100 / len,
			self = this;

		for (; i < len; i++) {
			setWidthForNodesContents(self.nodesContents, i, width);

			// init the code panel
			self.codePanels[i] = new self.Panel(
				self.nodesContents[i],
				self.contents[i],
				makeNotifierCallback(i),
				self.highlightModel[i]);
		}

		// a closure to put a scrolling position and code panel index together
		function makeNotifierCallback(panelIndex) {
			return function (pos, scrollWidth, lineIndex, startLine) {
				syncPanelsScrolling.call(self, pos, scrollWidth, lineIndex, startLine, panelIndex);
			}
		}
	};

	var jumpToFirstDiff = function () {
		this.codePanels[0].jumpToLine(this.diffModel[0][DIFF_KEYS.before_line_number] - 20);
		this.codePanels[1].jumpToLine(this.diffModel[0][DIFF_KEYS.after_line_number] - 20);
	};

	// creating easy-to-digest highlight model for code panel
	// looks like heretic stuff due to hard-coded diff_model
	var createHighlightModel = function (diffModel) {
		var beforeModel = {},
			afterModel = {},
			i = 0,
			len = diffModel.length,
			currDiff;

		for (; i < len; i++) {

			currDiff = diffModel[i];

			// add 'addspacer' if we have only added lines, to beforeModel
			// and 'add' lines to afterModel
			if (currDiff[DIFF_KEYS.before_line_count] === 0 && currDiff[DIFF_KEYS.after_line_count] !== 0) {
				addLines(
					currDiff[DIFF_KEYS.before_line_number],
					currDiff[DIFF_KEYS.before_line_count],
					LINE_STATUSES.added + LINE_STATUSES.spacer,
					beforeModel);

				addLines(
					currDiff[DIFF_KEYS.after_line_number],
					currDiff[DIFF_KEYS.after_line_count],
					LINE_STATUSES.added,
					afterModel);

				// add 'delspacer' if we have only deleted lines,  to afterModel
				// and 'del' lines to beforModel
			} else if (currDiff[DIFF_KEYS.after_line_count] === 0 && currDiff[DIFF_KEYS.before_line_count] !== 0) {

				addLines(
					currDiff[DIFF_KEYS.after_line_number],
					currDiff[DIFF_KEYS.after_line_count],
					LINE_STATUSES.deleted + LINE_STATUSES.spacer,
					afterModel);

				addLines(
					currDiff[DIFF_KEYS.before_line_number],
					currDiff[DIFF_KEYS.before_line_count],
					LINE_STATUSES.deleted,
					beforeModel);

				// if we have both counters, it means lines were modified, add 'mod' to both
			} else {
				addLines(
					currDiff[DIFF_KEYS.before_line_number],
					currDiff[DIFF_KEYS.before_line_count],
					LINE_STATUSES.modified,
					beforeModel);
				addLines(
					currDiff[DIFF_KEYS.after_line_number],
					currDiff[DIFF_KEYS.after_line_count],
					LINE_STATUSES.modified,
					afterModel);
			}
		}

		function addLines(from, delta, status, model) {
			var j = from;

			for (; j < from + delta; j++) {
				model[j] = status;
			}

			if (delta === 0) {
				model[j] = status;
			}
		}

		return {0: beforeModel, 1: afterModel}
	};


	var syncPanelsScrolling = function(pos, scrollWidth, lineIndex, startLine, panelIndex){
	    var i = 0,
			len = this.codePanels.length,
			fixedPos;

		if(panelIndex === 0){
			fixedPos = [pos[0] + this.beforePanelPosFix, pos[1]];
		} else {
			fixedPos = [pos[0] + this.afterPanelPosFix, pos[1]];
		}

		for(; i <len; i++){
			if(i !== panelIndex){
				this.codePanels[0].setScrollPosition(fixedPos);
			}
		}
	};

























	var getScrolledDiff = function (lineIndex, diffModel) {
		var i = 0,
			len = diffModel.length,
			deltaLines,
			beforeLineNumber;

		for (; i < len; i++) {

			deltaLines = diffModel[i][DIFF_KEYS.before_line_count] - diffModel[i][DIFF_KEYS.after_line_count];

			// if 'before lines count' === 'after lines count',  we don't need to fix position
			if (deltaLines === 0) continue;

			beforeLineNumber = diffModel[i][DIFF_KEYS.before_line_number];
			// if our current line is inside of the diff from any side
			if (lineIndex > beforeLineNumber && lineIndex < beforeLineNumber + Math.abs(deltaLines)){
				return deltaLines;
			}

		}

		return false;
	};

	var setWidthForNodesContents = function (node, i, width) {
		node[i].style.width = width + '%';
	};

	return DiffPanel;
})();