/**
 *  A set of helpful functions
 */

var H = {};


/**
 * Node appender
 * @param what {String}
 * @param where {HTMLElement}
 * @param append {Boolean} append or prepend
 * @return {Array}
 */

H.insertNode = function (what, where, append) {
	var div,
		elements = [],
		nodeList,
		node;

	div = document.createElement('div');
	div.innerHTML = what;
	nodeList = div.childNodes;

	for (var i = 0, len = nodeList.length; i < len; i++) {
		if (append) {
			node = where.appendChild(nodeList[0]);
		} else {
			node = where.insertBefore(nodeList[0], where.children[0]);
		}
		elements.push(node);
	}

	return elements;
};

H.deleteNode = function (what) {
	what.parentNode.removeChild(what);
};



/**
 * get scroll bar width for user environment
 * by http://www.alexandre-gomes.com/?p=115
 * @return {number}
 */
H.getScrollBarWidth = function () {
	var inner = document.createElement('p');
	inner.style.width = "100%";
	inner.style.height = "200px";

	var outer = document.createElement('div');
	outer.style.position = "absolute";
	outer.style.top = "0px";
	outer.style.left = "0px";
	outer.style.visibility = "hidden";
	outer.style.width = "200px";
	outer.style.height = "150px";
	outer.style.overflow = "hidden";
	outer.appendChild(inner);

	document.body.appendChild(outer);
	var w1 = inner.offsetWidth;
	outer.style.overflow = 'scroll';
	var w2 = inner.offsetWidth;
	if (w1 == w2) w2 = outer.clientWidth;

	document.body.removeChild(outer);

	return (w1 - w2);
};
