/**
 * Panel for displaying text with line numbers, keeping text formatting.
 * Copyright 2013 by Roman Khudyakov, roma.khudyakov@gmail.com
 *
 * Usage:
 * >>> var foo = new CodePanel(document.getElementById('id'), 'string\n string\n')
 *
 * depends form corresponding css file: CodePanel.css
 *
 *
 * 	TODO make scrolling over numbers to work
 *  TODO fix: macbook and ipad swipe scrolling on webkit
 *  TODO fix: zoom in, zoom out the browser window leaves blank lines
 *  TODO fix: add space to the very right of lines, when scrolling left
 */

var CodePanel = (function(){

	/**	a base css which will be added to all inner elements of html markup */
	var BASE_CSS_CLASS = 'code-panel';

	/** css classes for markup */
	var CSS_CLASSES = {
		wrapper: '__wrapper',

		//numbers
		number: '__number',
		numbers: '__numbers',
		numbersContainer: '__numbers-container',
		numbersFloater: '__numbers-floater',

		//lines
		line: '__line',
		lines: '__lines',
		linesContainer: '__lines-container',
		linesFloater: '__lines-floater',

		//colors
		added: '__added',
		deleted: '__deleted',
		modified: '__modified',

		//spacer
		spacer: '__line-spacer'
	};

	/** html for markup */
	var NODES = {
		bod: "<div class='" + BASE_CSS_CLASS + CSS_CLASSES.wrapper + "'>" +
				"<div class='" + BASE_CSS_CLASS + " clear'>" +
					"<div class='" + BASE_CSS_CLASS + CSS_CLASSES.numbers + "'><pre class='" + BASE_CSS_CLASS + CSS_CLASSES.numbersContainer + "'>" +
						"<code class='" + BASE_CSS_CLASS + CSS_CLASSES.numbersFloater + "'></code>" +
					"</pre></div>" +
					"<div class='" + BASE_CSS_CLASS + CSS_CLASSES.lines + "'><pre class='" + BASE_CSS_CLASS + CSS_CLASSES.linesContainer + "'>" +
						"<code class='" + BASE_CSS_CLASS + CSS_CLASSES.linesFloater + "'></code>" +
					"</pre></div>" +
				"</div>" +
			"</div>",

		// inner items
		number: "<span class='" + BASE_CSS_CLASS + CSS_CLASSES.number + "'></span>",
		numberAdded: "<span class='" + BASE_CSS_CLASS + CSS_CLASSES.number + " " + BASE_CSS_CLASS + CSS_CLASSES.added + "'></span>",
		numberDeleted: "<span class='" + BASE_CSS_CLASS + CSS_CLASSES.number + " " + BASE_CSS_CLASS + CSS_CLASSES.deleted + "'></span>",
		numberModified: "<div class='" + BASE_CSS_CLASS + CSS_CLASSES.number + " " + BASE_CSS_CLASS + CSS_CLASSES.modified + "'></div>",

		line: "<div class='" + BASE_CSS_CLASS + CSS_CLASSES.line + "'></div>",
		lineAdded: "<div class='" + BASE_CSS_CLASS + CSS_CLASSES.line + " " + BASE_CSS_CLASS + CSS_CLASSES.added + "'></div>",
		lineDeleted: "<div class='" + BASE_CSS_CLASS + CSS_CLASSES.line + " " + BASE_CSS_CLASS + CSS_CLASSES.deleted + "'></div>",
		lineModified: "<div class='" + BASE_CSS_CLASS + CSS_CLASSES.line + " " + BASE_CSS_CLASS + CSS_CLASSES.modified + "'></div>",
		lineAddedSpacer: "<div class='" + BASE_CSS_CLASS + CSS_CLASSES.line + " " + BASE_CSS_CLASS + CSS_CLASSES.added + " " + BASE_CSS_CLASS + CSS_CLASSES.spacer + "'></div>",
		lineDeletedSpacer: "<div class='" + BASE_CSS_CLASS + CSS_CLASSES.line + " " + BASE_CSS_CLASS + CSS_CLASSES.deleted + " " + BASE_CSS_CLASS + CSS_CLASSES.spacer + "'></div>"
	};

	/** map for escaping characters */
	var ENTRY_MAP = {
		"&": "&amp;",
		"<": "&lt;",
		">": "&gt;",
		'"': '&quot;',
		"'": '&#39;',
		"/": '&#x2F;'
	};

	// TODO move to config or overwrite with config
	var LINE_STATUSES = {
		added : 'add',
		deleted: 'del',
		modified: 'mod',
		spacer: 'spacer'
	};


	/** configuration */
	var config = {
		// height in pixels or 'auto' for full stretched;
		height: 'auto',
		// if true we skip highlight points in order to correct panel position in multi-panel view
		skipLines: true
	};

	/**
	 * CodePanel constructor
	 * @param HTMLElement {HTMLElement}
	 * @param data {String}
	 * @param notifier {Function}
	 * @param highlightModel {Object}
	 * @constructor
	 */
	function CodePanel(HTMLElement, data, notifier, highlightModel) {
		this.HTMLElement = HTMLElement;

		// add notifier about events
		if (!!notifier && typeof(notifier) === 'function') {
			this.notifier = notifier;
		}

		if (!!highlightModel) {
			this.highlightModel = highlightModel;
		}

		this.config = config;
		this.init(data);
	}

	CodePanel.prototype.init = function (data) {
		var self = this;

		// set some properties
		self.dataLines = createDataLines(data);
		self.dataLinesLength = self.dataLines.length;
		self.nodesLines = {};
		self.nodesNumbers = {};
		self.lineHeight = 0;
		self.startLineIndex = 0;
		self.visibleLines = 0;
		self.endLineIndex = 0;
		self.scrollBarWidth = H.getScrollBarWidth();
		self.preventNotifier = false;

		// create a html structure
		drawMarkup.call(self);

		// calculate the line height
		calcLineHeight.call(self);

		// set real height of panel to show actual scroll-control size
		makeRealHeight.call(self);

		// set a height of the panel from config, or full height if 'auto'
		setPanelHeight.call(self);

		// figure out initial state of start/end lines, draw lines
		calcLines.call(self);
		drawFullCode.call(self, self.startLineIndex, self.endLineIndex, true);

		// attach events for content
		self.scrollingEventCallback = function () {
			return scrollingEvent.call(self, self.startLineIndex);
		};

		self.nodeLines.addEventListener('scroll', self.scrollingEventCallback, false);

		// attach window resize event if needful
		if (self.config.height === 'auto') attachWindowResize.call(self);

		// if highlight model was set, jump to first highlight

		//self.jumpPoints = [[5437, 5446], [5500, 5510]];
	};

	/**
	 * Scroll to specific position, without triggering scrollingEvent event
	 * @param pos {Array} px to scroll top, left
	 */
	CodePanel.prototype.setScrollPosition = function (pos) {

		// check if whe already at the requested top, so don't scroll
		if (pos[0] !== this.nodeLines.scrollTop) {
			this.preventNotifier = true;
			this.nodeLines.scrollTop = pos[0];
		}

		// check if we have second parameter and it is not equal current scrolling position
		if (typeof(pos[1]) === 'number' && pos[1] !== this.nodeLines.scrollLeft) {
			this.preventNotifier = true;
			this.nodeLines.scrollLeft = pos[1];
		}
	};

	CodePanel.prototype.jumpToLine = function (line) {
		var height,
			heightDelta;

		if (line !== this.startLineIndex) {
			if (line < 0) line = 0;
			if (line > this.dataLinesLength - this.visibleLines) line = this.dataLinesLength - this.visibleLines;

			height = line * this.lineHeight;
			heightDelta = height - this.nodeLines.scrollTop;

			this.preventNotifier = true;
			this.nodeLines.scrollTop = height;

		}
		return heightDelta;
	};

	CodePanel.prototype.getScrollPosition = function(){
		return [this.nodeLines.scrollTop, this.nodeLines.scrollLeft];
	};

	CodePanel.prototype.adjustLinesContainerWidth = function(width){
		adjustNodeLinesWidth.call(this, width);
	};

	/**
	 * Draw whole code line with numbers
	 * @param from {Number} line to start
	 * @param to {Number} line to end
	 * @param append {Boolean} insert dom to the bottom or to the top
	 */
	var drawFullCode = function(from, to, append){
		if (from < 0) from = 0;
		if (to >= this.dataLinesLength - 1) to = this.dataLinesLength - 1;

		drawNumbers.call(this, from, to, append);
		drawLines.call(this, from, to, append, this.dataLines);
	};

	/**
	 * remove whole code line with numbers, clear dom and remove from objects
	 * @param from {Number}
	 * @param to {Number}
	 */
	var removeFullCode = function(from, to){
		if (from < 0) from = 0;
		if (to >= this.dataLinesLength) to = this.dataLinesLength - 1;

		var i = from;

		for(; i <= to; i++){
			H.deleteNode(this.nodesLines[i]);
			H.deleteNode(this.nodesNumbers[i])
		}

		while(from <= to){
			delete this.nodesLines[from];
			delete this.nodesNumbers[from];
			from++;
		}

	};

	/** calculation for lines  **/
	var calcLines = function(){
		//calcStartLine.call(this);
		calcVisibleLines.call(this);
	};

	var calcVisibleLines = function(){
		this.visibleLines = ~~(this.nodeLines.clientHeight / this.lineHeight);
		calcEndLine.call(this);
	};

	var calcStartLine = function(){
		this.startLineIndex = ~~(this.nodeLines.scrollTop / this.lineHeight);
	};

	var calcEndLine = function(){
		this.endLineIndex = this.startLineIndex + this.visibleLines;
	};

	var scrollingEvent = function(currLine){

		var	startScroll = this.startLineIndex,
			scrollDelta = 0,
			newCurrLine = currLine,
			halfIndex,
			skipDelta = 0,
			highLightedValue;

		// calculate new start line for panel
		calcStartLine.call(this);

		// check if we actually moved from the line
		if (newCurrLine !== this.startLineIndex) {
			newCurrLine = this.startLineIndex;

			// how many lines did we scroll
			scrollDelta = newCurrLine - startScroll;
			/*
			// activate skipping lines
			if(!!this.highlightModel && this.config.skipLines){

				halfIndex = newCurrLine + ~~(this.visibleLines / 2) - 5;

				//check if we cross highlighted line
				if( this.highlightModel[halfIndex] === LINE_STATUSES.deleted ||
					this.highlightModel[halfIndex] === LINE_STATUSES.added ||
					this.highlightModel[halfIndex] === LINE_STATUSES.modified) {

					// assign value
					highLightedValue = this.highlightModel[halfIndex];

					while(this.highlightModel[halfIndex++] && this.highlightModel[halfIndex++] === highLightedValue){
						skipDelta++;
					}
					// TODO this is quick fix, move to separate function
					if( scrollDelta > 0){
						this.nodeLines.scrollTop += skipDelta * this.lineHeight;
					} else {
						this.nodeLines.scrollTop -= (skipDelta + 1) * this.lineHeight;
					}

				}
			}
             */

			// scroll
			scroll.call(this, startScroll, scrollDelta);
		}

		// sync scroll with numbers
		this.nodeNumbers.scrollTop = this.nodeLines.scrollTop;

		// if preventNotifier
		if(this.preventNotifier){
			this.preventNotifier = false;
			return;
		}

		// update notifier about scroll position if needed
		if(!!this.notifier) {
			this.notifier([this.nodeLines.scrollTop, this.nodeLines.scrollLeft], this.nodeLines.scrollWidth, this.startLineIndex, scrollDelta);
		} else {
			// in order to display line width properly, adjust the width of container
			adjustNodeLinesWidth.call(this);
		}
	};

	/**
	 * scrolling the code panel
	 * @param startScroll
	 * @param scrollDelta
	 */
	var scroll = function(startScroll, scrollDelta){
		if(scrollDelta > 0){
			scrollDown.call(this, startScroll, scrollDelta);
		} else {
			scrollUp.call(this, startScroll, scrollDelta);
		}

	};

	var scrollDown = function(startScroll, scrollDelta, skipDelta){

		// draw scrolled lines
		drawFullCode.call(this, this.endLineIndex + 1,  this.endLineIndex + scrollDelta, true);

		// remove lines outside of the screen
		removeFullCode.call(this, startScroll, startScroll + scrollDelta - 1);

		// position code tag accordingly to scrolled pixels
		this.nodeLinesFloater.style.top = this.nodeLines.scrollTop + 'px';

		// sync scrolling with numbers
		this.nodeNumbersFloater.style.top = this.nodeLines.scrollTop + 'px';
		this.endLineIndex += scrollDelta;


	};

	var scrollUp = function(startScroll, scrollDelta, skipDelta){
		var deltaPos;

		drawFullCode.call(this, startScroll + scrollDelta, startScroll - 1, false);
		removeFullCode.call(this, this.endLineIndex + scrollDelta + 1, this.endLineIndex);

		deltaPos = this.nodeLines.scrollTop - this.lineHeight;
		this.nodeLinesFloater.style.top = deltaPos < 0 ?  0 : deltaPos + 'px';
		this.nodeNumbersFloater.style.top = deltaPos < 0 ?  0 : deltaPos + 'px';
		this.endLineIndex += scrollDelta;
	};

	var adjustNodeLinesWidth = function(width){
		if(typeof(width) === 'number'){
			this.nodeLinesContainer.style.width = width + 'px';
		} else {
			this.nodeLinesContainer.style.width = this.nodeLines.scrollWidth + 'px';
		}

	};

	/**
	 * attach resize on window in order to recalculate layout of the panel
	 */
	var attachWindowResize = function(){
		var self = this,
			oldEndLineIndex = 0,
			deltaEndLine = 0,
			timer = null;

		// on resize recalculate lines and draw/remove ones
		window.addEventListener('resize', function(){

			// pick old index for reference
			oldEndLineIndex = self.endLineIndex;

			if(timer == null){
				timer = setTimeout( function(){
					// set height of panel
					setPanelHeight.call(self);
					calcVisibleLines.call(self);

					// figure out if we expanded or stretched the browser window
					deltaEndLine = self.endLineIndex - oldEndLineIndex;

					// was added more space to the browser window, expanded
					if (deltaEndLine > 0){
						drawFullCode.call(self, oldEndLineIndex + 1, self.endLineIndex, true);

						// was reduces space from the browser window, stretched
					} else {
						removeFullCode.call(self, self.endLineIndex  + 1, oldEndLineIndex)
					}

					timer = null;
				}, 400);
			}
		});
	};

	/**
	 * calculate the height of the line to work with
	 */
    var calcLineHeight = function(){
		drawLines.call(this, 0, 0, true, ['test line']);
		this.lineHeight = this.nodesLines[0].clientHeight;
		H.deleteNode(this.nodesLines[0]);
		delete this.nodesLines[0];
    };

	/**
	 * preparing data to work with, escaping, and splitting in Array
	 * @param data {String}
	 * @return {Array}
	 */
	var createDataLines = function(data){
		return escapeHtml(data).split('\n');
	};

	/**
	 * draw content lines, split data by '\n' and create new line for each item
	 * @param from {Number}
	 * @param to {Number}
	 * @param append {Boolean} prepend or append items
	 * @param data {String} with data
	 */
	var drawLines = function(from, to, append, data){
		var i = from,
			j = to,
			self = this,
			nodesTpl = [NODES.line, NODES.lineAdded, NODES.lineDeleted, NODES.lineModified, NODES.lineAddedSpacer, NODES.lineDeletedSpacer],
			nodeTpl = nodesTpl[0],
			dataNode,
			lineNode;

		// if append is true we insert items from 0 ... 3 if false in opposite direction 3 ... 0
		if(append){
			for(; i <= j; i++ ){
				appender(i, append);
			}
		} else {
			for(; j >= i; j-- ){
				appender(j, append);
			}
		}

		function appender(index, append){
			dataNode = data[index] === '' ? '\n' : data[index];

			// figure out if we need to highlight lines or add spacers
			if(!!self.highlightModel && !!self.highlightModel[index]) {
				if(self.highlightModel[index] === LINE_STATUSES.added ) nodeTpl = nodesTpl[1];
				if(self.highlightModel[index] === LINE_STATUSES.deleted ) nodeTpl = nodesTpl[2];
				if(self.highlightModel[index] === LINE_STATUSES.modified ) nodeTpl = nodesTpl[3];

				// should insert tiny lines as pointers where lines were added/deleted,
				// turns out it's not as simple, so TODO create a place holders
				/*if(self.highlightModel[index] === LINE_STATUSES.added + LINE_STATUSES.spacer ) {
					nodeTpl = nodesTpl[4];
					//dataNode = '';
				}
				if(self.highlightModel[index] === LINE_STATUSES.deleted + LINE_STATUSES.spacer ) {
					nodeTpl = nodesTpl[5];
					//dataNode = '';
				} */
			} else {
				nodeTpl = nodesTpl[0];
			}

			lineNode = H.insertNode(nodeTpl, self.nodeLinesFloater, append)[0];
			lineNode.innerHTML = dataNode;
			self.nodesLines[index] = lineNode;
		}

	};

	/**
	 *  draw numbers
	 * @param from {Number}
	 * @param to {Number}
	 * @param append {Boolean}
	 */
	var drawNumbers = function(from, to, append){
		var i = from,
			j = to,
			self = this,
			nodesTpl = [NODES.number, NODES.numberAdded, NODES.numberDeleted, NODES.numberModified],
			nodeTpl = nodesTpl[0],
			numberNode;

		// if append is true we insert items from 0 ... 3 if false in opposite direction 3 ... 0
		if(append){
			for(; i <= j; i++ ){
				appender(i, append);
			}
		} else {
			for(; j >= i; j-- ){
				appender(j, append);
			}
		}

		function appender(index, append){

			// figure out if we need to highlight lines or add spacers
			if(!!self.highlightModel && !!self.highlightModel[index]) {
				if(self.highlightModel[index] === LINE_STATUSES.added ) nodeTpl = nodesTpl[1];
				if(self.highlightModel[index] === LINE_STATUSES.deleted ) nodeTpl = nodesTpl[2];
				if(self.highlightModel[index] === LINE_STATUSES.modified ) nodeTpl = nodesTpl[3];
			} else {
				nodeTpl = nodesTpl[0];
			}
			numberNode = H.insertNode(nodeTpl, self.nodeNumbersFloater, append)[0];
			numberNode.innerHTML = index + 1;
			self.nodesNumbers[index] = numberNode;
		}
	};

	/**
	 * draw a html markup
	 */
	var drawMarkup = function(){

		//create body
		this.nodeBod = H.insertNode(NODES.bod, this.HTMLElement, true)[0];

		// pick nodes
		// for numbers
		this.nodeNumbers = this.nodeBod.querySelector('.' + BASE_CSS_CLASS + CSS_CLASSES.numbers);
		this.nodeNumbersContainer = this.nodeBod.querySelector('.' + BASE_CSS_CLASS + CSS_CLASSES.numbersContainer);
		this.nodeNumbersFloater = this.nodeBod.querySelector('.' + BASE_CSS_CLASS + CSS_CLASSES.numbersFloater);

		// for lines
		this.nodeLines = this.nodeBod.querySelector('.' + BASE_CSS_CLASS + CSS_CLASSES.lines);
		this.nodeLinesContainer = this.nodeBod.querySelector('.' + BASE_CSS_CLASS + CSS_CLASSES.linesContainer);
		this.nodeLinesFloater = this.nodeBod.querySelector('.' + BASE_CSS_CLASS + CSS_CLASSES.linesFloater);
	};

	/**
	 * set a real height for nodes containers, in order to get "real" feel of scrolling process
	 */
	var makeRealHeight = function(){
		var height  =  this.dataLinesLength * this.lineHeight + 'px';

		this.nodeLinesContainer.style.height = height;
		this.nodeNumbersContainer.style.height = height;
	};

	/**
	 * set height of panels, if config.height is 'auto' the height will be full window, and it will adopt with resizing
	 */
	var setPanelHeight = function() {
		if(this.config.height === 'auto'){
			this.nodeLines.style.height = window.innerHeight + 'px';
			this.nodeNumbers.style.height = window.innerHeight - this.scrollBarWidth  + 'px';
		} else {
			this.nodeLines.style.height = this.config.height + this.scrollBarWidth + 'px';
			this.nodeNumbers.style.height = this.config.height + 'px';
		}
	};

	/**
	 * html escaper, courtesy https://github.com/janl/mustache.js/blob/master/mustache.js#L49
	 * @param string {String}
	 * @returns {String}
	 */

	var escapeHtml = function(string) {
		return String(string).replace(/[&<>"'\/]/g, function (s) {
			return ENTRY_MAP[s];
		});
	};

	return CodePanel;
})();
